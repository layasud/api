<?php

namespace App\Http\Controllers\Transactions;

use App\Http\Controllers\Controller;
use App\Models\Batch;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Cache;

/**
 * Batch controller
 * Class BatchController
 * @package App\Http\Controllers\Transactions
 */
class BatchController extends Controller
{
    /**
     * return all batches that match filter
     * @param Request $request
     * @return \Illuminate\Contracts\Routing\ResponseFactory|\Symfony\Component\HttpFoundation\Response
     */
    public function index(Request $request)
    {
        // cache key based on request input
        $key = encrypt(serialize($request->all()));

        // apply filters
        $query = null;

        if ( $request->has('date_from') ) {
            $query = $query
                ? $query->where('from','>=',Carbon::parse($request->input('date_from')))
                : Batch::where('from','>=',Carbon::parse($request->input('date_from'))) ;
        }

        if ( $request->has('date_to') ) {
            $query = $query
                ? $query->where('to','<=',Carbon::parse($request->input('date_to')))
                : Batch::where('to','<=',Carbon::parse($request->input('date_to'))) ;
        }

        $result = Cache::remember($key, 60, function () use ($query) {
            return $query ? $query->paginate(15) : Batch::paginate(15);
        });

        return response($result);
    }
}
