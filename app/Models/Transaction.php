<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * Transaction record
 * stores info about user and transaction amount
 * Class Transaction
 * @package App\Models
 */
class Transaction extends Model
{
    /**
     * The database table used by the model
     *
     * @var string
     */
    protected $table = 'transactions';

    /**
     * Model primary key
     *
     * @var string
     */
    protected $primaryKey = 'id';

    /**
     * The attributes that are mass assignable
     *
     * @var array
     */
    protected $fillable = [
        'user_id',
        'amount',
        'date',
        'batch_id'
    ];

    /**
     * Indicates if the model should be timestamped.
     *
     * @var bool
     */
    public $timestamps = false;

    /**
     * all batch transactions
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function transactions()
    {
        return $this->hasMany(Transaction::class,'batch_id','id');
    }
}
