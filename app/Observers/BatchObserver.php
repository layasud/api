<?php

namespace App\Observers;

use App\Models\Batch;
use Illuminate\Support\Facades\Cache;

class BatchObserver
{
    /**
     * Listen to the Batch created event.
     *
     * @param  Batch $batch
     * @return void
     */
    public function created(Batch $batch)
    {
        Cache::flush();
    }
}