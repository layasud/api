<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * All transactions are daily saved in a batch
 * Class Batch
 * @package App\Models
 */
class Batch extends Model
{
    /**
     * The database table used by the model
     *
     * @var string
     */
    protected $table = 'batches';

    /**
     * Model primary key
     *
     * @var string
     */
    protected $primaryKey = 'id';

    /**
     * The attributes that are mass assignable
     *
     * @var array
     */
    protected $fillable = [
        'amount',
        'from',
        'to',
    ];

    /**
     * Indicates if the model should be timestamped.
     *
     * @var bool
     */
    public $timestamps = false;

    /**
     * all batch transactions
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function transactions()
    {
        return $this->hasMany(Transaction::class,'batch_id','id');
    }
}
