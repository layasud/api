<?php

namespace App\Http\Controllers\Transactions;

use App\Http\Controllers\Controller;
use App\Http\Requests\TransactionRequest;
use App\Models\Transaction;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Cache;

class TransactionController extends Controller
{
    /**
     * @param Request $request
     * @return \Illuminate\Contracts\Routing\ResponseFactory|\Symfony\Component\HttpFoundation\Response
     */
    public function index(Request $request)
    {
        // cache key based on request input
        $key = encrypt(serialize($request->all()));

        // apply filters
        $query = null;
        if ( $request->has('date_from') ) {
            $query = $query
                ? $query->where('date','>=',Carbon::parse($request->input('date_from')))
                : Transaction::where('date','>=',Carbon::parse($request->input('date_from'))) ;
        }
        if ( $request->has('date_to') ) {
            $query = $query
                ? $query->where('date','<=',Carbon::parse($request->input('date_to')))
                : Transaction::where('date','<=',Carbon::parse($request->input('date_to'))) ;
        }
        if ( $request->has('amount_from') ) {
            $query = $query
                ? $query->where('amount','>=',$request->input('amount_from'))
                : Transaction::where('amount','>=',$request->input('amount_from')) ;
        }
        if ( $request->has('amount_to') ) {
            $query = $query
                ? $query->where('amount','<=',$request->input('amount_to'))
                : Transaction::where('amount','<=',$request->input('amount_to')) ;
        }
        if ( $request->has('user_id') ) {
            $query = $query
                ? $query->where('user_id',$request->input('user_id'))
                : Transaction::where('user_id',$request->input('user_id')) ;
        }

        $result = Cache::remember($key, 60, function () use ($query) {
            return $query ? $query->paginate(15) : Transaction::paginate(15);
        });

        return response($result);
    }

    /**
     * store new transaction
     * @param TransactionRequest $request
     * @return \Illuminate\Contracts\Routing\ResponseFactory|\Symfony\Component\HttpFoundation\Response
     */
    public function store(TransactionRequest $request)
    {
        $result = null;

        \DB::transaction(function()use(&$result,$request){
            $result = Transaction::create($request->all());
        });

        if ( !$result ) {
            abort(500,'Internal error');
        }

        return response('OK',201);
    }
}
