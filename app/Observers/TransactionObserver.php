<?php

namespace App\Observers;

use App\Models\Transaction;
use Illuminate\Support\Facades\Cache;

class TransactionObserver
{
    /**
     * Listen to the Batch created event.
     *
     * @param  Transaction $Transaction
     * @return void
     */
    public function created(Transaction $Transaction)
    {
        Cache::flush();
    }
}