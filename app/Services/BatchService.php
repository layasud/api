<?php

namespace App\Services;

use App\Models\Batch;
use App\Models\Transaction;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

/**
 * Batch service calculates new batch for the last day
 * Class BatchService
 * @package App\Services
 */
class BatchService extends Model
{
    /**
     * sum all transactions for the last day and save it as batch record
     */
    public static function calculateLastDay()
    {
        $from = Carbon::now()->addDays(-1);
        $to = Carbon::now();
        $transactions = Transaction::whereBetween('date',[$from,$to])->whereNull('batch_id')->get();
        $total = $transactions->sum('amount');
        $batch = Batch::create(['from'=>$from,'to'=>$to,'amount'=>$total]);
        $transactions->each->update(['batch_id'=>$batch->getKey()]);
    }
}
